import { Inject } from "typedi/types/decorators/inject.decorator";
import { Service } from "typedi/types/decorators/service.decorator";
import { OffensiveWord, OffensiveWordType } from "../entities/offensive-word.entity";
import { OffensiveWordRepository } from "../repositories/offensive-word.repository";

@Service()
export class OffensiveWordService {

	constructor(@Inject("OffensiveWordRepository") private offensiveWordRepository: OffensiveWordRepository) {}

	persist(offensiveWord: OffensiveWordType): void {

		const offensiveWordEntity = new OffensiveWord(offensiveWord);
		this.offensiveWordRepository.save(offensiveWordEntity);
		
	}
}
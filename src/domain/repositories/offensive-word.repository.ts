import { OffensiveWord } from "../entities/offensive-word.entity";

export interface OffensiveWordRepository {

    save(offensiveWord: OffensiveWord): void;

}
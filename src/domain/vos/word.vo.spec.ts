import {WordVO} from "./word.vo";

describe ("Word VO", () => {

	it("should crceate", () => {
		const newWord = "Word";
		const word: WordVO = WordVO.create(newWord);
		expect(word.value).toEqual(newWord);
	});

});
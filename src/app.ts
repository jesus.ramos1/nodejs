import "reflect-metadata";

import { CreateOffensiveWordUseCase } from "./application/usecases/create-offensive-word.usecase";
import { connectToDB } from "./infrastructure/config/mongo";
import { OffensiveWordRepositoryMongo } from "./infrastructure/repositories/offensive-word.repository.mongo";
import express from "express";
import { json } from "body-parser";
import { offensiveWordRouter } from "./infrastructure/routes/offensive-word.route";
import { Container } from "typedi/types/container.class";


Container.set("OffensiveWordRepository", new OffensiveWordRepositoryMongo());

connectToDB();

const useCase = Container.get(CreateOffensiveWordUseCase);
useCase.execute({word: "Nigger", level: 5});

console.log("App started");

const app = express();
app.use(json());
app.use(offensiveWordRouter);

app.listen(3000, ()=> {
	console.log("Server started");
});
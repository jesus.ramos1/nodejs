import { OffensiveWordRequest } from "./offensive-word.request";
import { OffensiveWordService } from "../../domain/services/offensive-word.service";
import { OffensiveWordType } from "../../domain/entities/offensive-word.entity";
import { IdVO } from "../../domain/vos/id.vo";
import { WordVO } from "../../domain/vos/word.vo";
import { LevelVO } from "../../domain/vos/level.vo";
import { Service } from "typedi";

@Service()
export class CreateOffensiveWordUseCase {

	constructor(private offensiveWordService: OffensiveWordService) {}

	execute(offensiveWordRequest: OffensiveWordRequest): void {
		const offensiveWordData: OffensiveWordType = {
			id: IdVO.create(),
			word: WordVO.create(offensiveWordRequest.word),
			level: LevelVO.create(offensiveWordRequest.level)
		};
		this.offensiveWordService.persist(offensiveWordData);
	}
}
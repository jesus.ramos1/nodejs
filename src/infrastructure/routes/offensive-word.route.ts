import express from "express";
import Container from "typedi";
import { CreateOffensiveWordUseCase } from "../../application/usecases/create-offensive-word.usecase";
import { OffensiveWordRequest } from "../../application/usecases/offensive-word.request";
import { OffensiveWordService } from "../../domain/services/offensive-word.service";
import { OffensiveWordRepositoryMongo } from "../repositories/offensive-word.repository.mongo";

const router = express.Router();

router.post("/api/offensive-word", (req, res) => {
	const {word, level} = req.body;
	const offensiveWordReques: OffensiveWordRequest = {
		word, level
	};
	const useCase = Container.get(CreateOffensiveWordUseCase);
	useCase.execute(offensiveWordReques);
	return res.status(201).send("Created!");
});

export {router as offensiveWordRouter};
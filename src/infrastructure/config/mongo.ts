import mongoose from "mongoose";

mongoose.set("strictQuery", false);

const connectToDB = async () => {
	try{
		const options = {
			authSource: "admin",
			auth: {
				username: "admin",
				password: "admin",
			},
		};

		await mongoose.connect("mongodb://localhost:27018/blog",  options);
	}catch(err) {
		console.log(err);
	}
};

export {connectToDB};